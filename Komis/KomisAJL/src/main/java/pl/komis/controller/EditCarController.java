/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.komis.controller;

import java.math.BigDecimal;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import pl.komis.ApplicationController;
import pl.komis.model.Brand;
import pl.komis.model.Car;
import pl.komis.model.CarType;
import pl.komis.model.Model;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "editCarController")
@RequestScoped
public class EditCarController {

    Car car = new Car();
    
    private String brand;
    private String model;
    private String VIN;
    private Integer year;
    private String carType;
    private String ocNumber;
    private String registrationDocument;
    private String fuel;
    private Integer mileage;
    private Integer power;
    private Integer capacity;
    private String transmission;
    private BigDecimal price;
    private String description;

    @ManagedProperty(value = "#{applicationController}")
    private ApplicationController applicationController;

    public ApplicationController getApplicationController() {
        return applicationController;
    }

    public void setApplicationController(ApplicationController applicationController) {
        this.applicationController = applicationController;
    }

    public EditCarController() {
    }

    public EditCarController(Car c) {
        this.car = c;
    }
    
    public void save() {
        EntityManager em = applicationController.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();

        Brand b = new Brand();
        Model m = new Model();
        CarType cT = new CarType();

        b.setName(this.brand);
        m.setName(this.model);
        m.setIdBrand(b);
        cT.setName(this.carType);

        car.setIdModel(m);
        car.setIdCarType(cT);
        car.setVIN(this.VIN);
        car.setYear(this.year);
        car.setOcNumber(this.ocNumber);
        car.setRegistrationDocument(this.registrationDocument);
        car.setFuel(this.fuel);
        car.setMileage(this.mileage);
        car.setPower(this.power);
        car.setCapacity(this.capacity);
        car.setTransmission(this.transmission);
        car.setPrice(this.price);
        car.setDescription(this.description);

        em.persist(this.car);
        transaction.commit();
        //addCar(car);
    }
    
    
    
    

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getOcNumber() {
        return ocNumber;
    }

    public void setOcNumber(String ocNumber) {
        this.ocNumber = ocNumber;
    }

    public String getRegistrationDocument() {
        return registrationDocument;
    }

    public void setRegistrationDocument(String registrationDocument) {
        this.registrationDocument = registrationDocument;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Integer getPower() {
        return power;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
    
    
    

}
