/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.komis.controller;

import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import pl.komis.model.Car;
import pl.komis.model.Model;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "carListController")
@RequestScoped
public class CarListController implements Serializable{

    private final EntityManagerFactory emf;
    private final EntityManager em;
    /**
     * Creates a new instance of CarListController
     */
    public CarListController() {
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }
    
    public EntityManager getEntityManager() {
        return em;
    }
    
    public List<Car> getList() {
        Query query = em.createQuery("from pl.komis.model.Car c");
    //    query.setParameter("", this.findModel(Long.MIN_VALUE))
        return query.getResultList();
    }
    
    public Model findModel(Long modelId){    
        return em.find(Model.class, modelId);
    }
    
    public String editCar(Car car){
        
        return null;
    }
    
//    public User findUser(Long userId) {
//        return em.find(User.class, userId);
//    }
//
//    @Override
//    public List<Rent> getRentBooks(Long l) {
//        Query query = em.createQuery("from Rent r where r.user = :user");
//        query.setParameter("user", this.findUser(l));
//        return query.getResultList();
//    }
    
}
