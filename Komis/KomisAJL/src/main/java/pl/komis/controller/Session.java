/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.komis.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import pl.komis.model.User;

/**
 *
 * @author RENT
 */

@ManagedBean(name = "sessionMy")
@SessionScoped
public class Session {
    
    private User user;
    private Boolean loggedIn;
    private String login;
    private String password;
    private String fullUserName;

    public String getFullUserName() {
        return fullUserName;
    }

    public void setFullUserName(String fullUserName) {
        this.fullUserName = fullUserName;
    }


    private final EntityManagerFactory emf;
    private final EntityManager em;

    public Session(){
            emf = Persistence.createEntityManagerFactory("pu");
            em = emf.createEntityManager();
    }
    
    public User getUser() {
        return user;
    }
    
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

//    public void setUser(User user) {
//        this.user = user;
//    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

//    public void setLoggedIn(Boolean loggedIn) {
//        this.loggedIn = loggedIn;
//    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    

    public void logIn(){
        Query query = em.createQuery("from User u where u.login = :login and u.password = :password");
        query.setParameter("login", this.login);
        query.setParameter("password", this.password);
        this.user = (User) query.getSingleResult();
        
        if (this.user != null){
            this.loggedIn = Boolean.TRUE;
            this.fullUserName = user.getFirstName() + " " + user.getLastName();
        }
    }
    
    public void logOut(){
        this.user = null;
        this.login = "";
        this.password = "";
        this.loggedIn = false;
        this.fullUserName = "";
    }

}
