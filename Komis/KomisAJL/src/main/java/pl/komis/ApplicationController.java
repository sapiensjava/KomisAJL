package pl.komis;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "applicationController")
@ApplicationScoped
public class ApplicationController {
    private final EntityManagerFactory emf;
    private final EntityManager em;

    public ApplicationController() {
        emf = Persistence.createEntityManagerFactory("pu");
        em = emf.createEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }
    
}
