/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.controllers.books;

import com.mycompany.Logic.BookBeanIfc;
import com.mycompany.model.Author;
import com.mycompany.model.Book;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author RENT
 */
@ManagedBean(name = "addBookController")
@RequestScoped
public class AddBookController {

    @EJB
    private BookBeanIfc bookBean;

    private String title;
    private String isbn;
    private int releaseDate;
    private String authors;

    /**
     * Creates a new instance of AddBookController
     */
    public AddBookController() {
    }

    public BookBeanIfc getBookBean() {
        return bookBean;
    }

    public void setBookBean(BookBeanIfc bookBean) {
        this.bookBean = bookBean;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(int releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public void save() {
        try {
            Book book = new Book();
            book.setIsbn(isbn);
            book.setReleaseDate(releaseDate);
            book.setTitle(title);
            
            String[] authorsTable = authors.split(",");
            ArrayList<Author> bookAuthors = new ArrayList<>();
            for(String name : authorsTable){
                Author author = new Author();
                author.setName(name);
                
                ArrayList<Book> arrayList = new ArrayList<Book>();
                arrayList.add(book);
                author.setBooks(arrayList);
                bookAuthors.add(author);
            }
            book.setAuthors(bookAuthors);
            bookBean.addBook(book);      
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
