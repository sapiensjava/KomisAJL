/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Logic;

import com.mycompany.model.Account;
import com.mycompany.model.Rent;
import com.mycompany.model.User;
import java.math.BigDecimal;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author RENT
 */
@Stateless
public class AccountingBean implements AccountingBeanIfc{
    
    @PersistenceContext(unitName = "persistance")
    private EntityManager em;

    @Override
    public void doDebit(Rent rent) {    
        Account account = new Account();
        account.setDebit(new BigDecimal(10));
        account.setDate(new Date());
        account.setUser(rent.getUser());
        em.persist(account);
    }

    @Override
    public void doCredit(User user, BigDecimal credit) {
        Account account = new Account();
        account.setCredit(credit);
        account.setDate(new Date());
        account.setUser(user);
        em.persist(account);
    }

    @Override
    public BigDecimal saldo(User user) {
        Query q = em.createQuery("select sum(a.credit) - sum(a.debit) from Account a where a.user = :user");
        Object singleResult = q.getSingleResult();
        return (BigDecimal) singleResult;
    }
    
}
