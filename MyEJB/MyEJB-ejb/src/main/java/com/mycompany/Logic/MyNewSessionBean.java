/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Logic;

import com.mycompany.model.Book;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
public class MyNewSessionBean implements MyNewSessionBeanRemote {

    @PersistenceContext(unitName = "persistance")
    private EntityManager em;

   //   private final ArrayList<String> books;
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public MyNewSessionBean() {
        //       this.books = new ArrayList<String>();
    }

    public List<Book> getBooks() {
        Query query= em.createQuery("from Book");
        return query.getResultList();
    }

    @Override
    public void addBook(String bookName) {
//        EntityTransaction transaction = em.getTransaction();
//        transaction.begin();
        
        Book book = new Book();
        book.setIsbn("1234989");
        book.setTitle(bookName);
        em.persist(book);
       
//        transaction.commit();
    }

}
