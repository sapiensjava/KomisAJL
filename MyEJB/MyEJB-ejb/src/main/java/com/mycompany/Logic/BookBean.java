/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Logic;

import com.mycompany.model.Author;
import com.mycompany.model.Book;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author RENT
 * Been 4 rodzaje
 * 
 */
@Stateless
public class BookBean implements BookBeanIfc{

    @EJB
    private AccountingBeanIfc abi;
    
    @PersistenceContext(unitName = "persistance")
    private EntityManager em;
    
    @Override    
    public Book addBook(Book book) {
        if (book != null) {
            em.persist(book);
            return book;
        }
        return null;
    }

    @Override
    public Author addAuthor(Author author) {        
        if (author != null) {
            em.persist(author);
            return author;
        }
        return null;
    }

    @Override
    public List<Book> getBooks() {
    
        try {
            Query createQuery = em.createQuery("from Book b");
            return createQuery.getResultList();
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ArrayList();
        }
    }
    
}
