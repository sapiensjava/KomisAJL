/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.Logic;

import com.mycompany.model.Book;
import com.mycompany.model.Rent;
import com.mycompany.model.User;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Remote;

/**
 *
 * @author RENT
 */
@Local
@Remote
public interface UserBeanIfc {    
    public User createUser(User user);
    public Book rentBook(Long userId, Long bookId);

    public User logIn(String username, String password);

    public User findUser(Long l);

    public List<Rent> getRentBooks(Long l);
}
